/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.abstractanimal;

/**
 *
 * @author user
 */
public class Earthworm extends Animal {

    private String species;

    public Earthworm(String species) {
        super("Earthworm", 0);
        this.species = species;
    }

    public void underground() {
        System.out.println("Earth worm: " + species + " live underground");
    }

    @Override
    public void eat() {
        System.out.println("Earth worm: " + species + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Earth worm: " + species + " move");
    }

    @Override
    public void speak() {
        System.out.println("Earth worm: " + species + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Earth worm: " + species + " sleep");
    }

}
