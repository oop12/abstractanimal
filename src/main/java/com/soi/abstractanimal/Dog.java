/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.abstractanimal;

/**
 *
 * @author user
 */
public class Dog extends LandAnimal {

    private String nickname;
    private String color;

    public Dog(String nickname, String color) {
        super("Dog", 4);
        this.nickname = nickname;
        this.color = color;
    }

    @Override
    public void run() {
        System.out.println("Dog: " + nickname + " Color: " + color + " run");
    }

    @Override
    public void eat() {
        System.out.println("Dog: " + nickname + " Color: " + color + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Dog: " + nickname + " Color: " + color + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog: " + nickname + " Color: " + color + " say: Box Box ");
    }

    @Override
    public void sleep() {
        System.out.println("Dog: " + nickname + " Color: " + color + " sleep");
    }

}
