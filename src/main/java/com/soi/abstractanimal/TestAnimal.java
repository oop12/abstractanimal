/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.abstractanimal;

/**
 *
 * @author user
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Sam");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));
        Animal a = h1;
        System.out.println("a == h1 ; a is reptile animal ? " + (a instanceof Reptile));
        Line();

        Cat c1 = new Cat("Zero", "Black");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));
        Animal b = c1;
        System.out.println("b == c1 ; b is aquatic animal ? " + (b instanceof AquaticAnimal));
        Line();

        Dog d1 = new Dog("Heng", "Brown");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));
        Animal c = d1;
        System.out.println("c == d1 ; c is poultry animal ? " + (c instanceof Poultry));
        Line();

        Crocodile k1 = new Crocodile("Kay");
        k1.eat();
        k1.walk();
        k1.crawl();
        k1.speak();
        k1.sleep();
        System.out.println("k1 is animal ? " + (k1 instanceof Animal));
        System.out.println("k1 is reptile animal ? " + (k1 instanceof Reptile));
        Animal d = k1;
        System.out.println("d == k1 ; d is poultry animal ? " + (d instanceof Poultry));
        Line();

        Snake s1 = new Snake("Nong Kiieow");
        s1.eat();
        s1.walk();
        s1.crawl();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is reptile animal ? " + (s1 instanceof Reptile));
        Animal e = s1;
        System.out.println("e == s1 ; e is aquatic animal ? " + (e instanceof AquaticAnimal));
        Line();

        Fish f1 = new Fish("Nong Tong");
        f1.eat();
        f1.walk();
        f1.swim();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animal ? " + (f1 instanceof AquaticAnimal));
        Animal f = f1;
        System.out.println("f == f1 ; f is poultry animal ? " + (f instanceof Poultry));
        Line();

        Crab p1 = new Crab("Bpoo");
        p1.eat();
        p1.walk();
        p1.swim();
        p1.speak();
        p1.sleep();
        System.out.println("p1 is animal ? " + (p1 instanceof Animal));
        System.out.println("p1 is aquatic animal ? " + (p1 instanceof AquaticAnimal));
        Animal g = p1;
        System.out.println("g == p1 ; g is reptile animal ? " + (g instanceof Reptile));
        Line();

        Bat b1 = new Bat("MeeNoi");
        b1.eat();
        b1.walk();
        b1.fly();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is poultry animal ? " + (b1 instanceof Poultry));
        Animal h = b1;
        System.out.println("h == b1 ; h is reptile animal ? " + (h instanceof Reptile));
        Line();

        Bird bb1 = new Bird("Som");
        bb1.eat();
        bb1.walk();
        bb1.fly();
        bb1.speak();
        bb1.sleep();
        System.out.println("bb1 is animal ? " + (bb1 instanceof Animal));
        System.out.println("bb1 is poultry animal ? " + (bb1 instanceof Poultry));
        Animal i = bb1;
        System.out.println("i == bb1 ; i is aquatic animal ? " + (i instanceof AquaticAnimal));
        Line();

        Earthworm e1 = new Earthworm("Eisenia foetida");
        e1.eat();
        e1.walk();
        e1.underground();
        e1.speak();
        e1.sleep();
        System.out.println("e1 is animal ? " + (e1 instanceof Animal));
        Animal j = e1;
        System.out.println("j == e1 ; j is land animal ? " + (j instanceof LandAnimal));
        System.out.println("j == e1 ; j is aquatic animal ? " + (j instanceof AquaticAnimal));
        Line();

        Plane plane = new Plane("Engine number 1");
        plane.starEngine();
        plane.raiseSpeed();
        plane.run();
        plane.fly();
        Line();

        Car car = new Car("Engine number 2");
        car.starEngine();
        car.raiseSpeed();
        car.run();
        Line();

        Flyable[] flyable = {b1, bb1, plane};
        for (Flyable x : flyable) {
            if (x instanceof Plane) {
                Plane p = (Plane) x;
                p.starEngine();
                p.raiseSpeed();
                p.run();
            }
            x.fly();
        }
        Line();

        Runable[] runable = {h1, d1, c1, plane, car};
        for (Runable r : runable) {
            r.run();
        }
        Line();

    }

    private static void Line() {
        System.out.println("---------------------");
    }

}
