/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.abstractanimal;

/**
 *
 * @author user
 */
public class Cat extends LandAnimal {

    private String nickname;
    private String color;

    public Cat(String nickname, String color) {
        super("Cat", 4);
        this.nickname = nickname;
        this.color = color;
    }

    @Override
    public void run() {
        System.out.println("Cat: " + nickname + " Color: " + color + " run");
    }

    @Override
    public void eat() {
        System.out.println("Cat: " + nickname + " Color: " + color + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Cat: " + nickname + " Color: " + color + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Cat: " + nickname + " Color: " + color + " say: meow meow ");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: " + nickname + " Color: " + color + " sleep");
    }

}
